package examples;
// https://www.softwaretestinghelp.com/integration-of-jenkins-with-selenium-webdriver/

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import examples.Constants;

public class jenkins_demo
{
    public String url = "http://google.com";
    // String driverPath = "/home/sandeep/JavaExamples/chromedriver";
    public WebDriver driver;

    @Test
    public void testgooglesearch() {
        // System.setProperty("webdriver.gecko.driver", Constants.driverPath);
        System.setProperty("webdriver.chrome.driver", Constants.driverPath);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
        chromeOptions.addArguments("disable-infobars"); // disabling infobars
        chromeOptions.addArguments("--disable-extensions"); // disabling extensions
        chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
        chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
        
        driver = new ChromeDriver(chromeOptions);
        driver.get (url);
        String Expectedtitle = "Google";
        String Actualtitle = driver.getTitle();
        System.out.println("Before Assertion " + Expectedtitle + " " + Actualtitle);
        Assert.assertEquals(Actualtitle, Expectedtitle);
        System.out.println("After Assertion " + Expectedtitle + " " + Actualtitle + " Title matched ");
        driver.quit();
    }
}

